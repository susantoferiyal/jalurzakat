package com.poltek.stario.util;

import android.content.Context;
import android.util.Log;

import com.poltek.stario.model.DataZakatModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class FloydWarshallUtil {

    public static HashMap<String, String> shortestPath(List<DataZakatModel> orgData, DataZakatModel data) {
        orgData.add(0, new DataZakatModel(data.getId(), data.getName(), data.getMuzaki(), data.getMustahiq(), data.getTotal(), data.getStatus(), data.getLatlng()));

        HashMap<String, String> maps = new HashMap<>();

        HashMap<String, HashMap<String, Double>> test = new HashMap<>();
        for (int i = 0; i < orgData.size(); i++) {
            HashMap<String, Double> hashMap = new HashMap<>();
            DataZakatModel modela = orgData.get(i);
            for (int j = 0; j < orgData.size(); j++) {
                DataZakatModel modelb = orgData.get(j);

                String dataa = modela.getLatlng();
                String datab = modelb.getLatlng();

                double lata = Double.parseDouble(dataa.split(":")[0]);
                double lnga = Double.parseDouble(dataa.split(":")[1]);
                double latb = Double.parseDouble(datab.split(":")[0]);
                double lngb = Double.parseDouble(datab.split(":")[1]);

                double distance = distance(lata, lnga, latb, lngb);
                hashMap.put(modelb.getId(), distance);
            }
            test.put(modela.getId(), hashMap);
        }

        List<String> idFilter = new ArrayList<>();
        String idTemp = "";
        double disTemp = 9999;
        for (String key : test.get(data.getId()).keySet()) {
            double distance = test.get(data.getId()).get(key);
            if (!key.equals(data.getId())) {
                if (distance < disTemp) {
                    idTemp = key;
                    disTemp = distance;
                }
            }
        }

        Log.d("YEAH", data.getId() + "->" + idTemp + " = " + disTemp);
        test.get(idTemp).remove(data.getId());
        test.remove(data.getId());
        idFilter.add(data.getId());
        idFilter.add(idTemp);

        for (String key : test.keySet()) {
            test.get(key).remove(data.getId());
        }

        while (test.size() > 1) {
            String idTemp2 = "";
            disTemp = 9999;
            for (String key : test.get(idTemp).keySet()) {
                double distance = test.get(idTemp).get(key);
                if (!key.equals(idTemp))
                    if (distance < disTemp) {
                        idTemp2 = key;
                        disTemp = distance;
                    }
            }
            Log.d("YEAH", idTemp + "->" + idTemp2 + " = " + disTemp);
            test.get(idTemp2).remove(idTemp);
            test.remove(idTemp);

            for (String key : test.keySet()) {
                test.get(key).remove(idTemp);
            }

            idTemp = idTemp2;
            idFilter.add(idTemp);
        }

        DataZakatModel modela = null;
        DataZakatModel modelb = null;
        int urutan = 0;
        for (int i = 0; i < idFilter.size() - 1; i++) {
            for (DataZakatModel model : orgData) {
                if (model.getId().equals(idFilter.get(i))) {
                    modela = model;
                    break;
                }
            }
            for (DataZakatModel model : orgData) {
                if (model.getId().equals(idFilter.get(i + 1))) {
                    modelb = model;
                    break;
                }
            }

            String dataa = modela.getLatlng();
            String datab = modelb.getLatlng();

            double lata = Double.parseDouble(dataa.split(":")[0]);
            double lnga = Double.parseDouble(dataa.split(":")[1]);
            double latb = Double.parseDouble(datab.split(":")[0]);
            double lngb = Double.parseDouble(datab.split(":")[1]);

            double distance = distance(lata, lnga, latb, lngb);
            maps.put("data" + urutan, toJson(modela, modelb, distance));
            urutan++;
        }

        Log.d("TAGTAG", maps.toString());
        return maps;
    }

    public static HashMap<String, String> determineMapPath(Context context, List<DataZakatModel> orgData, DataZakatModel data) {
        //Tambah ke data yang akan di proses di posisi ke 0, kenapa ? karena buat ngitung awal lokasi user ke masjid lain
        orgData.add(0, new DataZakatModel(data.getId(), data.getName(), data.getMuzaki(), data.getMustahiq(), data.getTotal(), data.getStatus(), data.getLatlng()));

        //Data penampung setelah di filter / urutkan
        ArrayList<DataZakatModel> filterList = new ArrayList<>();
        //Variable temporary untuk mengambil jarak terdekat
        double disTemp = -1;
        //Variable temporary untuk menampung id masjid
        String idTemp = "-1";
        int urutan = -1;
        HashMap<String, String> maps = new HashMap<>();
        for (int i = 0; i < orgData.size() - 1; i++) {
            disTemp = -1;
            //Ketika data masih kosong, maka masuk ke iterasi dibawah
            //atau ketika data user id == id temporary -> id masjid dengan jarak terpendek, maka akan mengambil data tujuan
            if (i == 0 || orgData.get(i).getId().equals(idTemp)) {
                DataZakatModel modela = orgData.get(i);
                DataZakatModel shortestB = null;
                for (int j = i + 1; j < orgData.size(); j++) {
                    DataZakatModel modelb = orgData.get(j);
                    String dataa = modela.getLatlng();
                    String datab = modelb.getLatlng();

                    //Pengambilan latitude dan longitude user asal ke tujuan
                    double lata = Double.parseDouble(dataa.split(":")[0]);
                    double lnga = Double.parseDouble(dataa.split(":")[1]);
                    double latb = Double.parseDouble(datab.split(":")[0]);
                    double lngb = Double.parseDouble(datab.split(":")[1]);

                    //Menghitung jarak udara masjid a ke masjid b
                    double distance = distance(lata, lnga, latb, lngb);
                    Log.d("FLOYD", modela.getName() + " -> " + modelb.getName() + " = " + distance);
                    //jika disTemp == -1 maka dia dianggap belum terisi atau iterasi awal
                    if (disTemp == -1) {
                        disTemp = distance;
                        idTemp = modelb.getId();
                        shortestB = modelb;
                    } else {
                        //jika disTemp > 0 dan distemp lebih besar dari pada jarak udara yang dihitung, maka update isi dari variable distemp
                        if (distance < disTemp) {
                            disTemp = distance;
                            idTemp = modelb.getId();
                            shortestB = modelb;
                        }
                    }
                }
                urutan++;
                maps.put("data" + urutan, toJson(modela, shortestB, disTemp));
            } else {
                //Adalah ketika user id != id temporary maka continue / lompati iterasi ke i
                continue;
            }
        }

        return maps;
    }

    public static String toJson(DataZakatModel data1, DataZakatModel data2, double distance) {
        JSONObject object = new JSONObject();
        try {
            object.put("distance", distance);
            JSONArray array1 = new JSONArray();
            JSONObject object1 = new JSONObject(data1.toString());
            array1.put(object1);

            JSONArray array2 = new JSONArray();
            JSONObject object2 = new JSONObject(data2.toString());
            array2.put(object2);

            object.put("data1", array1);
            object.put("data2", array2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAGTAG", "json " + object.toString());
        return object.toString();
    }

    public static ArrayList<DataZakatModel> determinePath(Context context, List<DataZakatModel> orgData) {
        String id = ZakatSharedPreferences.getSharedPreference(context, "id_masjid", "-1");
        String nama = ZakatSharedPreferences.getSharedPreference(context, "nama_masjid", "-1");
        String alamat = ZakatSharedPreferences.getSharedPreference(context, "alamat_masjid", "-1");
        String latlng = ZakatSharedPreferences.getSharedPreference(context, "latlng_masjid", "-1");
        orgData.add(0, new DataZakatModel(id, nama, "0", "0", "0", "0", latlng));

        ArrayList<DataZakatModel> filterList = new ArrayList<>();
        double disTemp = -1;
        String idTemp = "-1";
        DataZakatModel dataTemp = null;
        HashMap<String, Double> maps = new HashMap<>();
        for (int i = 0; i < orgData.size() - 1; i++) {
            disTemp = -1;
            if (i == 0 || orgData.get(i).getId().equals(idTemp)) {
                for (int j = i + 1; j < orgData.size(); j++) {
                    DataZakatModel modela = orgData.get(i);
                    DataZakatModel modelb = orgData.get(j);
                    String dataa = modela.getLatlng();
                    String datab = modelb.getLatlng();

                    double lata = Double.parseDouble(dataa.split(":")[0]);
                    double lnga = Double.parseDouble(dataa.split(":")[1]);
                    double latb = Double.parseDouble(datab.split(":")[0]);
                    double lngb = Double.parseDouble(datab.split(":")[1]);

                    double distance = distance(lata, lnga, latb, lngb);
                    maps.put(modela.getName() + ":" + modelb.getName(), distance);
                    if (disTemp == -1) {
                        disTemp = distance;
                        dataTemp = modelb;
                        idTemp = modelb.getId();
                    } else {
                        if (distance < disTemp) {
                            disTemp = distance;
                            dataTemp = modelb;
                            idTemp = modelb.getId();
                        }
                    }
                }
                filterList.add(dataTemp);
            } else {
                continue;
            }
        }

        return filterList;
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        final int R = 6371;
        Double latDistance = deg2rad(lat2 - lat1);
        Double lonDistance = deg2rad(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        Double d = R * c;

        return d;
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static LinkedHashMap sortHashMapByValuesD(HashMap passedMap) {
        List mapKeys = new ArrayList(passedMap.keySet());
        List mapValues = new ArrayList(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap sortedMap = new LinkedHashMap();

        Iterator valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                String comp1 = passedMap.get(key).toString();
                String comp2 = val.toString();

                if (comp1.equals(comp2)) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    sortedMap.put((String) key, (Double) val);
                    break;
                }

            }

        }
        return sortedMap;
    }
}
