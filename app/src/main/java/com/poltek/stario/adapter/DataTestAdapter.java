package com.poltek.stario.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.poltek.stario.R;
import com.poltek.stario.model.DataZakatModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataTestAdapter extends RecyclerView.Adapter<DataTestAdapter.ViewHolder> {

    private Context mContext;
    private String[] mKeys;
    private HashMap<String, Double> mList = new HashMap<String, Double>();
    private OnItemClickListener mListener = null;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txvw_from, txvw_where, txvw_distance;

        public ViewHolder(View view) {
            super(view);
            txvw_from = view.findViewById(R.id.txvw_from);
            txvw_where = view.findViewById(R.id.txvw_where);
            txvw_distance = view.findViewById(R.id.txvw_distance);
        }
    }

    public DataTestAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_test, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        String key = mKeys[i];
        double value = mList.get(key);

        viewHolder.txvw_from.setText(key.split(":")[0]);
        viewHolder.txvw_where.setText(key.split(":")[1]);
        viewHolder.txvw_distance.setText(String.format("%.2f", value));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(HashMap<String, Double> list) {
        mList = list;
        mKeys = mList.keySet().toArray(new String[mList.size()]);
        notifyDataSetChanged();
    }

    public static interface OnItemClickListener{
        public void onItemClickListener(View view, int position);
    }

    public void setCustomOnClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }
}
