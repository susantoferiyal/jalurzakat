package com.poltek.stario.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.poltek.stario.R;
import com.poltek.stario.model.DataZakatModel;

import java.util.ArrayList;
import java.util.List;

public class DataZakatAdapter extends RecyclerView.Adapter<DataZakatAdapter.ViewHolder> {

    private Context mContext;
    private List<DataZakatModel> mList = new ArrayList<>();
    private OnItemClickListener mListener = null;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout lnly_item;
        public TextView txvw_masjid, txvw_muzakki, txvw_mustahiq, txvw_total, txvw_status;

        public ViewHolder(View view) {
            super(view);
            lnly_item = view.findViewById(R.id.lnly_item);
            txvw_masjid = view.findViewById(R.id.txvw_masjid);
            txvw_muzakki = view.findViewById(R.id.txvw_muzakki);
            txvw_mustahiq = view.findViewById(R.id.txvw_mustahiq);
            txvw_total = view.findViewById(R.id.txvw_total);
            txvw_status = view.findViewById(R.id.txvw_status);
        }
    }

    public DataZakatAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_data_zakat, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        DataZakatModel data = mList.get(i);
        viewHolder.txvw_masjid.setText(data.getName());
        viewHolder.txvw_mustahiq.setText(data.getMustahiq());
        viewHolder.txvw_status.setText(data.getStatus());
        viewHolder.txvw_muzakki.setText(data.getMuzaki());
        viewHolder.txvw_total.setText(data.getTotal());

        if (data.getStatus().toLowerCase().equals("kelebihan")) {
            viewHolder.txvw_muzakki.setTextColor(mContext.getResources().getColor(R.color.MediumSeaGreen));
        } else {
            viewHolder.txvw_muzakki.setTextColor(mContext.getResources().getColor(R.color.Red));
        }

        if (mListener != null) {
            viewHolder.lnly_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickListener(view, i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<DataZakatModel> list) {
        mList = list;
        Log.d("TAGTAG", "mList " + mList.size());
        notifyDataSetChanged();
    }

    public static interface OnItemClickListener{
        public void onItemClickListener(View view, int position);
    }

    public void setCustomOnClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }
}
