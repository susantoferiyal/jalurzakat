package com.poltek.stario.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.poltek.stario.R;
import com.poltek.stario.model.MessageModel;
import com.poltek.stario.util.ZakatSharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context mContext;
    private List<MessageModel> list = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txvw_title, txvw_subtitle, txvw_date;

        public ViewHolder(View view) {
            super(view);
            txvw_title = view.findViewById(R.id.txvw_title);
            txvw_subtitle = view.findViewById(R.id.txvw_subtitle);
            txvw_date = view.findViewById(R.id.txvw_date);
        }
    }

    public MessageAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_message, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        MessageModel data = list.get(i);

        String title = data.getNama_mas_beri().toLowerCase().contains("masjid") ? data.getNama_mas_beri() : "Masjid " + data.getNama_mas_beri();
        String description = "Melakukan pembagian zakat sebanyak " + data.getZakat_masuk() + " kepada " + (data.getNama_mas_terima().toLowerCase().contains("masjid") ? data.getNama_mas_terima() : "Masjid " + data.getNama_mas_terima());
        String tanggal = data.getTanggal();

        viewHolder.txvw_title.setText(title);
        viewHolder.txvw_subtitle.setText(description);
        viewHolder.txvw_date.setText(tanggal);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<MessageModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
