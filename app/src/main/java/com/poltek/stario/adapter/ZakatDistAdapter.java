package com.poltek.stario.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.poltek.stario.R;

import java.util.ArrayList;
import java.util.List;

public class ZakatDistAdapter extends BaseAdapter {

    public ZakatDistAdapter(Context context) {
        mContext = context;
    }

    Context mContext;
    List<ArrayList<String>> mList = new ArrayList<>();

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_zakat_dist, null);

        TextView txvw_nama = v.findViewById(R.id.txvw_nama);
        TextView txvw_kekurangan = v.findViewById(R.id.txvw_kekurangan);
        TextView txvw_menerima = v.findViewById(R.id.txvw_menerima);

        String name = mList.get(i).get(0).toString();
        String kekurangan = mList.get(i).get(1).toString();
        txvw_nama.setText(
                name
        );
        txvw_kekurangan.setText(
                kekurangan
        );
        txvw_menerima.setText(mList.get(i).get(2));

        return v;
    }

    public void setList(List<ArrayList<String>> list) {
        mList = list;
        notifyDataSetChanged();
    }
}
