package com.poltek.stario.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.poltek.stario.R;

import java.util.ArrayList;
import java.util.List;

public class ZakatDetailAdapter extends BaseAdapter {

    public ZakatDetailAdapter(Context context) {
        mContext = context;
    }

    Context mContext;
    List<ArrayList<String>> mList = new ArrayList<>();

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_zakat_detail, null);

        TextView txvw_nama = v.findViewById(R.id.txvw_nama);
        TextView txvw_jarak = v.findViewById(R.id.txvw_jarak);

        String name = mList.get(i).get(0).toString();
        String jarak = mList.get(i).get(1).toString();
        txvw_nama.setText(name);
        txvw_jarak.setText(jarak);

        return v;
    }

    public void setList(List<ArrayList<String>> list) {
        mList = list;
        notifyDataSetChanged();
    }
}
