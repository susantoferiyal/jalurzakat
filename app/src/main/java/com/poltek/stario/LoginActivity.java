package com.poltek.stario;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends Activity {

    private TextInputEditText tiet_masjid;
    private TextInputEditText tiet_password;
    private Button bttn_login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tiet_masjid = findViewById(R.id.tiet_masjid);
        tiet_password = findViewById(R.id.tiet_password);
        bttn_login = findViewById(R.id.bttn_login);

        bttn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLoginButton();
            }
        });
    }

    private void handleLoginButton() {
        String id = tiet_masjid.getText().toString();
        String password = tiet_password.getText().toString();

        if (id.isEmpty()) {
            tiet_masjid.setError("Silahkan masukkan ID masjid anda");
        } else if (password.isEmpty()) {
            tiet_masjid.setError("Silahkan masukkan password anda");
        } else {
            execLogin(id, password);
        }
    }

    private void execLogin(String id, String password) {
        RequestParams params = new RequestParams();
        params.add("id_masjid", id);
        params.add("password", password);

        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Loading. . .");

        RestClient.post(LoginActivity.this, "login", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        String message = data.getString("message");
                        if (data.getJSONObject("data") != null) {
                            String id = data.getJSONObject("data").getString("id_mas");
                            String nama = data.getJSONObject("data").getString("nama_mas");
                            String alamat = data.getJSONObject("data").getString("alamat_mas");
                            String latlng = data.getJSONObject("data").getString("koordinat_mas");

                            ZakatSharedPreferences.setSharedpreferences(LoginActivity.this, "id_masjid", id);
                            ZakatSharedPreferences.setSharedpreferences(LoginActivity.this, "nama_masjid", nama);
                            ZakatSharedPreferences.setSharedpreferences(LoginActivity.this, "alamat_masjid", alamat);
                            ZakatSharedPreferences.setSharedpreferences(LoginActivity.this, "latlng_masjid", latlng);

                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            finish();
                        }

                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                String result = new String(responseBody);
                Log.d("TAGTAG", statusCode + " code");
                dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });
    }
}
