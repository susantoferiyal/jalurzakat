package com.poltek.stario.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.poltek.stario.R;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.infowindow.InfoWindow;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import cz.msebera.android.httpclient.Header;

public class OsmMapFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private MapView mapView;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private double minLatitude = 0;
    private double maxLatitude = 0;
    private double minLongitude = 0;
    private double maxLongitude = 0;
    private double centerLatitude = 0;
    private double centerLongitude = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setupMap() {
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);

        mapView.getController().setZoom(15);
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        MyLocationNewOverlay oMapLocationOverlay = new MyLocationNewOverlay(mapView);
        mapView.getOverlays().add(oMapLocationOverlay);

        CompassOverlay compassOverlay = new CompassOverlay(getActivity(), mapView);
        compassOverlay.enableCompass();
        mapView.getOverlays().add(compassOverlay);

        getMasjidList();
    }

    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void setCenterInMyCurrentLocation() {
        if (mLastLocation != null) {
            mapView.getController().setCenter(new GeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
        } else {
            Toast.makeText(getActivity(), "Getting current location", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_osm_map, container, false);
        mapView = view.findViewById(R.id.map);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        setupMap();

        return view;
    }

    public void getMasjidList() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.get(getActivity(), "masjid", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        JSONArray datas = data.getJSONArray("data");
                        for (int x = 0; x < datas.length(); x++) {
                            String name = ((JSONObject) datas.get(x)).getString("nama_mas");
                            String address = ((JSONObject) datas.get(x)).getString("alamat_mas");
                            String latlng = ((JSONObject) datas.get(x)).getString("koordinat_mas");
                            LatLng location = new LatLng(Double.parseDouble(latlng.split(":")[0]),
                                    Double.parseDouble(latlng.split(":")[1]));

                            double latitude = Double.parseDouble(latlng.split(":")[0]);
                            double longitude = Double.parseDouble(latlng.split(":")[1]);

                            if (minLatitude == 0) {
                                minLatitude = latitude;
                            } else {
                                if (latitude < minLatitude) {
                                    minLatitude = latitude;
                                }
                            }

                            if (maxLatitude == 0) {
                                maxLatitude = latitude;
                            } else {
                                if (latitude > maxLatitude) {
                                    maxLatitude = latitude;
                                }
                            }

                            if (minLongitude == 0) {
                                minLongitude = longitude;
                            } else {
                                if (longitude < minLongitude) {
                                    minLongitude = longitude;
                                }
                            }

                            if (maxLongitude == 0) {
                                maxLongitude = longitude;
                            } else {
                                if (longitude > maxLongitude) {
                                    maxLongitude = longitude;
                                }
                            }

                            String userLatlng = ZakatSharedPreferences.getSharedPreference(getActivity(), "latlng_masjid", "-1");

                            Marker marker = new Marker(mapView);
                            marker.setPosition(new GeoPoint(latitude, longitude));
                            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                            if (userLatlng.equals(latlng))
                                marker.setIcon(getActivity().getResources().getDrawable(R.drawable.ic_room_green));
                            else
                                marker.setIcon(getActivity().getResources().getDrawable(R.drawable.ic_room_blue));
                            marker.setTitle(name);
                            mapView.getOverlays().add(marker);
                        }

                        centerLatitude = (minLatitude + maxLatitude) / 2;
                        centerLongitude = (minLongitude + maxLongitude) / 2;
                        mapView.getController().setCenter(new GeoPoint(centerLatitude, centerLongitude));

                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }
}
