package com.poltek.stario.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.R;
import com.poltek.stario.adapter.ZakatDistAdapter;
import com.poltek.stario.model.DataZakatModel;
import com.poltek.stario.rest.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import cz.msebera.android.httpclient.Header;

public class PerhitunganZakatActivity extends Activity {

    private ListView lsvw_data;
    private ZakatDistAdapter mAdapter;

    private TextView txvw_leftname;
    private TextView txvw_leftqty;
    private Button bttn_distribute;

    List<ArrayList<String>> tmpList = new ArrayList<>();
    int sisaZakat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perh_zakat);

        Intent i = getIntent();

        DataZakatModel myData = (DataZakatModel) i.getSerializableExtra("mydata");
        final String userId = myData.getId();
        String userLatlng = myData.getLatlng();
        final String userName = myData.getName();
        final String userMuzaki = myData.getMuzaki();
        final String userMustahiq = myData.getMustahiq();
        final String userTotal = myData.getTotal();
        sisaZakat = Integer.parseInt(myData.getTotal());
        List<ArrayList<String>> list = new ArrayList<>();
        try {
            JSONObject dataObject = new JSONObject(i.getStringExtra("data"));
            Log.d("TAGTAG", "data " + dataObject.toString());
            int urutan = 0;
            Iterator<String> iter = dataObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                JSONObject object = new JSONObject(dataObject.getString("data" + urutan));

                try {
                    String distance = object.getString("distance");
                    String id = object.getJSONArray("data2").getJSONObject(0).getString("id");
                    String name = object.getJSONArray("data2").getJSONObject(0).getString("name");
                    String kekurangan = object.getJSONArray("data2").getJSONObject(0).getString("total");
                    String mustahiq = object.getJSONArray("data2").getJSONObject(0).getString("mustahiq");
                    String muzaki = object.getJSONArray("data2").getJSONObject(0).getString("muzaki");
                    String latlng = object.getJSONArray("data2").getJSONObject(0).getString("latlng");
                    if (sisaZakat > 0) {
                        ArrayList<String> data = new ArrayList<>();
                        data.add(name);
                        data.add(kekurangan);

                        if (sisaZakat < Integer.parseInt((kekurangan.substring(1, kekurangan.length())))) {
                            data.add(sisaZakat + "");
                            sisaZakat += Integer.parseInt(kekurangan);
                        } else {
                            int sisa = sisaZakat + Integer.parseInt(kekurangan);
                            data.add(Integer.parseInt((kekurangan.substring(1, kekurangan.length()))) + "");
                            sisaZakat = sisa;
                        }
                        data.add(id);
                        data.add(mustahiq);
                        data.add(muzaki);
                        data.add(latlng);
                        data.add(distance);

                        Log.d("HEHE", "data " + data.toString());
                        list.add(data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                urutan++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        lsvw_data = findViewById(R.id.lsvw_data);
        mAdapter = new ZakatDistAdapter(PerhitunganZakatActivity.this);
        lsvw_data.setAdapter(mAdapter);
        txvw_leftname = findViewById(R.id.txvw_leftname);
        txvw_leftqty = findViewById(R.id.txvw_leftqty);
        bttn_distribute = findViewById(R.id.bttn_distribute);

        txvw_leftname.setText("Sisa Zakat Masjid " + userName);
        txvw_leftqty.setText((sisaZakat < 0 ? 0 : sisaZakat) + " Zakat");

        mAdapter.setList(list);
        tmpList.addAll(list);

        bttn_distribute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tmpList.size() > 0) {
                    JSONArray data = new JSONArray();
                    for (int x = 0; x < tmpList.size(); x++) {
                        String id = tmpList.get(x).get(3);
                        int muzaki = Integer.parseInt(tmpList.get(x).get(2)) + Integer.parseInt(tmpList.get(x).get(5));
                        int total = Integer.parseInt(tmpList.get(x).get(2)) + Integer.parseInt(tmpList.get(x).get(1));
                        String status = total > 0 ? "Lebih" : total == 0 ? "Normal" : "Kurang";

                        JSONObject object = new JSONObject();
                        try {
                            object.put("id_penerima", id);
                            object.put("muzaki", muzaki);
                            object.put("mustahiq", tmpList.get(x).get(4));
                            object.put("total", total);
                            object.put("status", status);
                            object.put("menerima", tmpList.get(x).get(2));
                            object.put("namaTerima", tmpList.get(x).get(0));
                            object.put("namaBeri", userName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        data.put(object);
                    }

                    Log.d("TAGTAG", "data " + data.toString());
                    RequestParams params = new RequestParams();
                    params.add("id_mas", userId);
                    params.add("nama_mas", userName);
                    params.add("muzaki_mas", userMuzaki);
                    params.add("mustahiq_mas", (Integer.parseInt(userMustahiq) + Integer.parseInt(userTotal)) + "");
                    params.add("total_mas", (sisaZakat < 0 ? 0 : sisaZakat) + "");
                    params.add("status_mas", sisaZakat < 0 ? "Normal" : "Lebih");
                    params.add("masjid", data.toString());
                    Log.d("TAGTAG", "params " + params.toString());

                    RestClient.post(PerhitunganZakatActivity.this, "proseszakat", params, new AsyncHttpResponseHandler() {
                        final ProgressDialog dialog = new ProgressDialog(PerhitunganZakatActivity.this);

                        @Override
                        public void onStart() {
                            super.onStart();
                            dialog.setMessage("Loading. . .");
                            dialog.show();
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String result = new String(responseBody);
                            Log.d("TAGTAG", result);
                            if (statusCode == 200) {
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();

                                Intent intent = new Intent(PerhitunganZakatActivity.this, DataDetailZakatActivity.class);
                                intent.putExtra("data", getIntent().getStringExtra("data"));
                                intent.putExtra("mydata", getIntent().getSerializableExtra("mydata"));
                                intent.putExtra("totaldata", tmpList.size());
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            String result = new String(responseBody);
                            Log.d("TAGTAG", result);
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                        }
                    });
                } else {
                    Toast.makeText(PerhitunganZakatActivity.this, "Anda tidak dapat melakukan distribusi, silahkan coba beberapa saat lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
