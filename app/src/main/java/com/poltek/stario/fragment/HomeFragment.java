package com.poltek.stario.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.R;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class HomeFragment extends Fragment {

    TextView txvw_name;
    TextView txvw_muzaki;
    TextView txvw_mustahiq;
    TextView txvw_total;
    TextView txvw_status;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);

        txvw_name = view.findViewById(R.id.txvw_name);
        txvw_muzaki = view.findViewById(R.id.txvw_muzaki);
        txvw_mustahiq = view.findViewById(R.id.txvw_mustahiq);
        txvw_total = view.findViewById(R.id.txvw_total);
        txvw_status = view.findViewById(R.id.txvw_status);

        getData();

        return view;
    }

    public void getData() {
        RequestParams params = new RequestParams();
        params.add("id_masjid", "1");

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.post(getActivity(), "datazakat", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        String id = ZakatSharedPreferences.getSharedPreference(getActivity(), "id_masjid", "-1");
                        JSONObject data = new JSONObject(result);
                        JSONArray datas = data.getJSONArray("data");
                        for (int x = 0; x < datas.length(); x++) {
                            if (((JSONObject) datas.get(x)).getString("id").equals(id)) {
                                txvw_name.setText(((JSONObject) datas.get(x)).getString("name"));
                                txvw_muzaki.setText(((JSONObject) datas.get(x)).getString("muzakki"));
                                txvw_mustahiq.setText(((JSONObject) datas.get(x)).getString("mustahiq"));
                                txvw_total.setText(((JSONObject) datas.get(x)).getString("total"));
                                txvw_status.setText(((JSONObject) datas.get(x)).getString("status"));
                                break;
                            }
                        }
                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }
}
