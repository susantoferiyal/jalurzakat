package com.poltek.stario.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hendrix.pdfmyxml.PdfDocument;
import com.hendrix.pdfmyxml.viewRenderer.AbstractViewRenderer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.PdfViewerActivity;
import com.poltek.stario.R;
import com.poltek.stario.adapter.MessageAdapter;
import com.poltek.stario.model.MessageModel;
import com.poltek.stario.model.ReportModel;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MessageFragment extends Fragment {

    private RelativeLayout rvly_layout;
    private MessageAdapter mAdapter;
    private SwipeRefreshLayout srly_list;
    private RecyclerView rcvw_content;
    List<MessageModel> mList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_zakat, null);

        setHasOptionsMenu(true);

        rvly_layout = view.findViewById(R.id.rvly_layout);
        srly_list = view.findViewById(R.id.srly_list);
        mAdapter = new MessageAdapter(getActivity());
        rcvw_content = view.findViewById(R.id.rcvw_content);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rcvw_content.setLayoutManager(mLayoutManager);
        rcvw_content.setItemAnimator(new DefaultItemAnimator());
        rcvw_content.setAdapter(mAdapter);

        rvly_layout.setDrawingCacheEnabled(true);
        rvly_layout.buildDrawingCache();

        srly_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(false);
            }
        });
        getData(true);

        return view;
    }

    private void getData(final boolean refresh) {
        mList.clear();
        RequestParams params = new RequestParams();
        params.add("id_masjid", ZakatSharedPreferences.getSharedPreference(getActivity(), "id_masjid", "-1"));

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.post(getActivity(), "transaction", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                if (refresh)
                    dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        JSONArray datas = data.getJSONArray("data");
                        for (int x = 0; x < datas.length(); x++) {
                            MessageModel model = new MessageModel(
                                    ((JSONObject) datas.get(x)).getString("id_trans"),
                                    ((JSONObject) datas.get(x)).getString("id_zakat"),
                                    ((JSONObject) datas.get(x)).getString("tipe_trans"),
                                    ((JSONObject) datas.get(x)).getString("zakat_keluar"),
                                    ((JSONObject) datas.get(x)).getString("zakat_masuk"),
                                    ((JSONObject) datas.get(x)).getString("mas_terima"),
                                    ((JSONObject) datas.get(x)).getString("nama_mas_terima"),
                                    ((JSONObject) datas.get(x)).getString("mas_beri"),
                                    ((JSONObject) datas.get(x)).getString("nama_mas_beri"),
                                    ((JSONObject) datas.get(x)).getString("tanggal")
                            );
                            mList.add(model);
                        }
                        if (refresh)
                            dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (refresh)
                    dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Log.d("TAGTAG", "size " + mList.size());
                mAdapter.setList(mList);
                if (!refresh)
                    srly_list.setRefreshing(false);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_download, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_download) {
            generatePdf();
        }

        return super.onOptionsItemSelected(item);
    }

    private HashMap<String, List<ReportModel>> mReports = new HashMap<>();

    public void generatePdf() {
        mReports.clear();
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.get(getActivity(), "report", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        JSONArray datas = data.getJSONArray("data");
                        for (int x = 0; x < datas.length(); x++) {
                            List<ReportModel> reports = new ArrayList<>();
                            ReportModel model = new ReportModel(
                                    ((JSONObject) datas.get(x)).getString("mas_beri"),
                                    ((JSONObject) datas.get(x)).getString("alamat_mas"),
                                    ((JSONObject) datas.get(x)).getString("muzaki"),
                                    ((JSONObject) datas.get(x)).getString("mustahiq"),
                                    ((JSONObject) datas.get(x)).getString("total"),
                                    ((JSONObject) datas.get(x)).getString("status"),
                                    ((JSONObject) datas.get(x)).getString("zakat_masuk"),
                                    ((JSONObject) datas.get(x)).getString("nama_mas_terima"),
                                    ((JSONObject) datas.get(x)).getString("nama_mas_beri"),
                                    ((JSONObject) datas.get(x)).getString("tanggal")
                            );
                            reports.add(model);
                            if (mReports.size() == 0) {
                                mReports.put(model.getMas_beri(), reports);
                            } else {
                                if (mReports.get(model.getMas_beri()) != null) {
                                    mReports.get(model.getMas_beri()).add(model);
                                } else {
                                    mReports.put(model.getMas_beri(), reports);
                                }
                            }
                        }
                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mReports.size() > 0) {
                    PdfDocument doc = new PdfDocument(getActivity());
                    for (final String key : mReports.keySet()) {
                        AbstractViewRenderer page = new AbstractViewRenderer(getActivity(), R.layout.activity_pdf) {
                            @Override
                            protected void initView(View view) {
                                ReportModel parent = null;
                                for (ReportModel modelParent : mReports.get(key)) {
                                    if (key == modelParent.getMas_beri()) {
                                        parent = modelParent;
                                        break;
                                    }
                                }

                                LinearLayout content = view.findViewById(R.id.lnly_content);

                                TextView header = new TextView(getActivity());
                                header.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                header.setText("JALUR ZAKAT Kelurahan Mojolangu");
                                header.setTextColor(getResources().getColor(android.R.color.black));
                                header.setTextSize(16);
                                header.setTypeface(header.getTypeface(), Typeface.BOLD);
                                header.setGravity(Gravity.CENTER);

                                content.addView(header);

                                View divider = new View(getActivity());
                                divider.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 4));
                                divider.setPadding(0, 8, 0, 0);
                                divider.setBackgroundColor(getResources().getColor(android.R.color.black));

                                content.addView(divider);

                                TextView masjid = new TextView(getActivity());
                                masjid.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                masjid.setPadding(0, 16, 0, 0);
                                masjid.setText(parent.getNama_mas_beri());
                                masjid.setTextColor(getResources().getColor(android.R.color.black));
                                masjid.setTextSize(14);
                                masjid.setTypeface(masjid.getTypeface(), Typeface.BOLD);

                                content.addView(masjid);

                                TextView alamat = new TextView(getActivity());
                                alamat.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                alamat.setPadding(0, 4, 0, 0);
                                alamat.setText(parent.getAlamat_mas());
                                alamat.setTextColor(getResources().getColor(android.R.color.darker_gray));
                                alamat.setTextSize(12);

                                content.addView(alamat);

                                LinearLayout status = new LinearLayout(getActivity());
                                status.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                status.setPadding(32, 8, 0, 0);
                                status.setOrientation(LinearLayout.HORIZONTAL);
                                status.setWeightSum(1);

                                TextView labelStatus = new TextView(getActivity());
                                LinearLayout.LayoutParams lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lblStatusParams.weight = 0.7f;
                                labelStatus.setLayoutParams(lblStatusParams);
                                labelStatus.setText("Status");
                                labelStatus.setTextColor(getResources().getColor(android.R.color.black));
                                labelStatus.setTypeface(labelStatus.getTypeface(), Typeface.BOLD);

                                status.addView(labelStatus);

                                labelStatus = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                labelStatus.setLayoutParams(lblStatusParams);
                                labelStatus.setText(":");
                                labelStatus.setTextColor(getResources().getColor(android.R.color.black));
                                labelStatus.setPadding(0, 0, 8, 0);

                                status.addView(labelStatus);

                                labelStatus = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lblStatusParams.weight = 0.3f;
                                labelStatus.setLayoutParams(lblStatusParams);
                                labelStatus.setText(parent.getStatus());
                                labelStatus.setTextColor(getResources().getColor(android.R.color.black));

                                status.addView(labelStatus);

                                content.addView(status);

                                LinearLayout muzaki = new LinearLayout(getActivity());
                                muzaki.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                muzaki.setPadding(32, 8, 0, 0);
                                muzaki.setOrientation(LinearLayout.HORIZONTAL);
                                muzaki.setWeightSum(1);

                                TextView labelMuzaki = new TextView(getActivity());
                                lblStatusParams.weight = 0.7f;
                                labelMuzaki.setLayoutParams(lblStatusParams);
                                labelMuzaki.setText("Muzaki");
                                labelMuzaki.setTextColor(getResources().getColor(android.R.color.black));
                                labelMuzaki.setTypeface(labelMuzaki.getTypeface(), Typeface.BOLD);

                                muzaki.addView(labelMuzaki);

                                labelMuzaki = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                labelMuzaki.setLayoutParams(lblStatusParams);
                                labelMuzaki.setText(":");
                                labelMuzaki.setTextColor(getResources().getColor(android.R.color.black));
                                labelMuzaki.setPadding(0, 0, 8, 0);

                                muzaki.addView(labelMuzaki);

                                labelMuzaki = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lblStatusParams.weight = 0.3f;
                                labelMuzaki.setLayoutParams(lblStatusParams);
                                labelMuzaki.setText(parent.getMuzaki());
                                labelMuzaki.setTextColor(getResources().getColor(android.R.color.black));

                                muzaki.addView(labelMuzaki);

                                content.addView(muzaki);

                                LinearLayout mustahiq = new LinearLayout(getActivity());
                                mustahiq.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                mustahiq.setPadding(32, 8, 0, 0);
                                mustahiq.setOrientation(LinearLayout.HORIZONTAL);
                                mustahiq.setWeightSum(1);

                                TextView labelMustahiq = new TextView(getActivity());
                                lblStatusParams.weight = 0.7f;
                                labelMustahiq.setLayoutParams(lblStatusParams);
                                labelMustahiq.setText("Mustahiq");
                                labelMustahiq.setTextColor(getResources().getColor(android.R.color.black));
                                labelMustahiq.setTypeface(labelMustahiq.getTypeface(), Typeface.BOLD);

                                mustahiq.addView(labelMustahiq);

                                labelMustahiq = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                labelMustahiq.setLayoutParams(lblStatusParams);
                                labelMustahiq.setText(":");
                                labelMustahiq.setTextColor(getResources().getColor(android.R.color.black));
                                labelMustahiq.setPadding(0, 0, 8, 0);

                                mustahiq.addView(labelMustahiq);

                                labelMustahiq = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lblStatusParams.weight = 0.3f;
                                labelMustahiq.setLayoutParams(lblStatusParams);
                                labelMustahiq.setText(parent.getMustahiq());
                                labelMustahiq.setTextColor(getResources().getColor(android.R.color.black));

                                mustahiq.addView(labelMustahiq);

                                content.addView(mustahiq);

                                LinearLayout mengirim = new LinearLayout(getActivity());
                                mengirim.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                mengirim.setPadding(32, 8, 0, 0);
                                mengirim.setOrientation(LinearLayout.HORIZONTAL);
                                mengirim.setWeightSum(1);

                                TextView labelMengirim = new TextView(getActivity());
                                lblStatusParams.weight = 0.7f;
                                labelMengirim.setLayoutParams(lblStatusParams);
                                labelMengirim.setText("Mengirim");
                                labelMengirim.setTextColor(getResources().getColor(android.R.color.black));
                                labelMengirim.setTypeface(labelMengirim.getTypeface(), Typeface.BOLD);

                                mengirim.addView(labelMengirim);

                                labelMengirim = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                labelMengirim.setLayoutParams(lblStatusParams);
                                labelMengirim.setText(":");
                                labelMengirim.setTextColor(getResources().getColor(android.R.color.black));
                                labelMengirim.setPadding(0, 0, 8, 0);

                                mengirim.addView(labelMengirim);

                                labelMengirim = new TextView(getActivity());
                                lblStatusParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lblStatusParams.weight = 0.3f;
                                labelMengirim.setLayoutParams(lblStatusParams);
                                labelMengirim.setText("");
                                labelMengirim.setTextColor(getResources().getColor(android.R.color.black));

                                mengirim.addView(labelMengirim);

                                content.addView(mengirim);

                                int position = 1;
                                for (ReportModel report : mReports.get(key)) {
                                    mengirim = new LinearLayout(getActivity());
                                    mengirim.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                    mengirim.setPadding(64, 8, 0, 0);
                                    mengirim.setOrientation(LinearLayout.HORIZONTAL);
                                    mengirim.setWeightSum(1);

                                    labelMengirim = new TextView(getActivity());
                                    lblStatusParams.weight = 0.5f;
                                    labelMengirim.setLayoutParams(lblStatusParams);
                                    labelMengirim.setText(position + ". " + report.getNama_mas_terima());
                                    labelMengirim.setTextColor(getResources().getColor(android.R.color.black));

                                    mengirim.addView(labelMengirim);

                                    labelMengirim = new TextView(getActivity());
                                    lblStatusParams.weight = 0.5f;
                                    labelMengirim.setLayoutParams(lblStatusParams);
                                    labelMengirim.setText("(" + report.getZakat_masuk() + ")");
                                    labelMengirim.setTextColor(getResources().getColor(android.R.color.black));

                                    mengirim.addView(labelMengirim);

                                    content.addView(mengirim);
                                    position++;
                                }
                            }
                        };

//                        page.setReuseBitmap(true);
                        doc.addPage(page);
                    }
                    doc.setRenderWidth(1480);
                    doc.setRenderHeight(2508);
                    doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
                    doc.setProgressTitle(R.string.gen_please_wait);
                    doc.setProgressMessage(R.string.gen_pdf_file);
                    doc.setFileName("download_pdf_" + System.currentTimeMillis());
                    doc.setSaveDirectory(Environment.getExternalStorageDirectory());
                    doc.setInflateOnMainThread(false);
                    doc.setListener(new PdfDocument.Callback() {
                        @Override
                        public void onComplete(final File file) {
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }
                            builder.setTitle("Download Berhasil")
                                    .setMessage("Apakah anda ingin membuka file pdf ?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(getActivity(), PdfViewerActivity.class);
                                            intent.putExtra("path", file.getAbsolutePath());
                                            startActivity(intent);
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(R.drawable.ic_check_white)
                                    .show();
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.i(PdfDocument.TAG_PDF_MY_XML, Log.getStackTraceString(e));
                        }
                    });

                    doc.createPdf(getActivity());
                } else {
                    Toast.makeText(getActivity(), "Generate PDF Gagal", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
