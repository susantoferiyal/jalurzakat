package com.poltek.stario.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.R;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class InputZakatFragment extends Fragment {

    public InputZakatFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private EditText edtx_muzakki;
    private EditText edtx_mustahiq;
    private Button bttn_save;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_zakat, null);

        edtx_muzakki = view.findViewById(R.id.edtx_muzakki);
        edtx_mustahiq = view.findViewById(R.id.edtx_mustahiq);
        bttn_save = view.findViewById(R.id.bttn_save);

        bttn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSaveButton();
            }
        });

        return view;
    }

    private void handleSaveButton() {
        final String muzakki = edtx_muzakki.getText().toString();
        final String mustahiq = edtx_mustahiq.getText().toString();

        if (muzakki.isEmpty()) {
            Toast.makeText(getActivity(), "Silahkan inputkan jumlah Muzakki anda", Toast.LENGTH_SHORT).show();
        } else if (mustahiq.isEmpty()) {
            Toast.makeText(getActivity(), "Silahkan inputkan jumlah mustahiq anda", Toast.LENGTH_SHORT).show();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setMessage("Anda yakin akan melakukan penambahan data ?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            execSave(muzakki, mustahiq);
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }).show();
        }
    }

    private void execSave(String muzakki, String mustahiq) {
        RequestParams params = new RequestParams();
        params.add("id_mas", ZakatSharedPreferences.getSharedPreference(getActivity(), "id_masjid", "-1"));
        params.add("muzakki", muzakki);
        params.add("mustahiq", mustahiq);

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.post(getActivity(), "inputzakat", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        Toast.makeText(getActivity(), data.getString("message"), Toast.LENGTH_SHORT).show();
                        clearForm();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                String result = new String(responseBody);
                Log.d("TAGTAG", statusCode + " code");
                dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });
    }

    private void clearForm() {
        edtx_mustahiq.setText("0");
        edtx_muzakki.setText("0");
    }
}
