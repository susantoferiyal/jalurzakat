package com.poltek.stario.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.R;
import com.poltek.stario.adapter.DataZakatAdapter;
import com.poltek.stario.model.DataZakatModel;
import com.poltek.stario.rest.RestClient;
import com.poltek.stario.util.FloydWarshallUtil;
import com.poltek.stario.util.ZakatSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DataZakatFragment extends Fragment {

    public DataZakatFragment() {

    }

    private SwipeRefreshLayout srly_list;
    private RecyclerView rcvw_content;
    private Button bttn_process;
    private DataZakatAdapter mAdapter;

    private List<DataZakatModel> mList = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        getData(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_zakat, null);

        srly_list = view.findViewById(R.id.srly_list);
        bttn_process = view.findViewById(R.id.bttn_process);
        mAdapter = new DataZakatAdapter(getActivity());

        rcvw_content = view.findViewById(R.id.rcvw_content);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rcvw_content.setLayoutManager(mLayoutManager);
        rcvw_content.setItemAnimator(new DefaultItemAnimator());
        rcvw_content.setAdapter(mAdapter);

        srly_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(false);
            }
        });

        bttn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoDetail();
            }
        });

        return view;
    }

    public void gotoDetail() {
        DataZakatModel myData = null;
        String myId = ZakatSharedPreferences.getSharedPreference(getActivity(), "id_masjid", "-1");
        List<DataZakatModel> filter = new ArrayList<>();
        List<DataZakatModel> filter2 = new ArrayList<>();
        for (DataZakatModel model : mList) {
            if (model.getId().equals(myId)) {
                myData = model;
            }

            if (!model.getId().equals(myId) && model.getStatus().toLowerCase().equals("kurang")) {
                filter.add(model);
            }
        }

        HashMap<String, String> datas = FloydWarshallUtil.shortestPath(filter, myData);
        JSONObject object = new JSONObject(datas);
        Log.d("TAGTAG", "data " + object.toString());
        Intent intent = new Intent(getActivity(), PerhitunganZakatActivity.class);
        intent.putExtra("data", object.toString());
        intent.putExtra("mydata", myData);
        startActivity(intent);
    }

    public void getData(final boolean refresh) {
        mList.clear();

        RequestParams params = new RequestParams();
        params.add("id_masjid", "1");

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading. . .");

        RestClient.post(getActivity(), "datazakat", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (refresh)
                    dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d("TAGTAG", result);
                if (statusCode == 200) {
                    try {
                        JSONObject data = new JSONObject(result);
                        JSONArray datas = data.getJSONArray("data");
                        for (int x = 0; x < datas.length(); x++) {
                            DataZakatModel model = new DataZakatModel();
                            model.setId(((JSONObject) datas.get(x)).getString("id"));
                            model.setName(((JSONObject) datas.get(x)).getString("name"));
                            model.setMustahiq(((JSONObject) datas.get(x)).getString("mustahiq"));
                            model.setMuzaki(((JSONObject) datas.get(x)).getString("muzakki"));
                            model.setStatus(((JSONObject) datas.get(x)).getString("status"));
                            model.setTotal(((JSONObject) datas.get(x)).getString("total"));
                            model.setLatlng(((JSONObject) datas.get(x)).getString("latlng"));
                            mList.add(model);
                        }
                        if (refresh)
                            dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (refresh)
                    dialog.dismiss();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Log.d("TAGTAG", "size " + mList.size());
                mAdapter.setList(mList);
                if (!refresh)
                    srly_list.setRefreshing(false);
            }
        });
    }
}
