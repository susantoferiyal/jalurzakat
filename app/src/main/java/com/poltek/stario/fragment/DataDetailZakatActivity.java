package com.poltek.stario.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.poltek.stario.R;
import com.poltek.stario.adapter.ZakatDetailAdapter;
import com.poltek.stario.model.DataZakatModel;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class DataDetailZakatActivity extends Activity {

    private TextView txvw_leftname;
    private TextView txvw_leftqty;

    private ListView lsvw_data;
    private ZakatDetailAdapter mAdapter;

    private MapView mapView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data);

        Intent i = getIntent();
        try {
            int totaldata = i.getIntExtra("totaldata", 0);
            JSONObject dataObject = new JSONObject(i.getStringExtra("data"));
            txvw_leftname = findViewById(R.id.txvw_leftname);
            txvw_leftqty = findViewById(R.id.txvw_leftqty);
            mapView = findViewById(R.id.map);
            lsvw_data = findViewById(R.id.lsvw_data);
            mAdapter = new ZakatDetailAdapter(DataDetailZakatActivity.this);
            lsvw_data.setAdapter(mAdapter);

            setupMap(dataObject, totaldata);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupMap(JSONObject dataObject, int totaldata) throws JSONException {
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);

        mapView.getController().setZoom(15);
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        MyLocationNewOverlay oMapLocationOverlay = new MyLocationNewOverlay(mapView);
        mapView.getOverlays().add(oMapLocationOverlay);
        oMapLocationOverlay.enableMyLocation();
        oMapLocationOverlay.enableFollowLocation();

        CompassOverlay compassOverlay = new CompassOverlay(DataDetailZakatActivity.this, mapView);
        compassOverlay.enableCompass();
        mapView.getOverlays().add(compassOverlay);

        DataZakatModel myData = (DataZakatModel) getIntent().getSerializableExtra("mydata");
        String userLatlng = myData.getLatlng();
        String name = myData.getName();

        txvw_leftname.setText("Sisa Zakat Masjid " + name);
        txvw_leftqty.setText(myData.getTotal() + " Zakat");

        Marker marker = new Marker(mapView);
        marker.setPosition(new GeoPoint(Double.parseDouble(userLatlng.split(":")[0]), Double.parseDouble(userLatlng.split(":")[1])));
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(DataDetailZakatActivity.this.getResources().getDrawable(R.drawable.ic_room_green));
        marker.setTitle(name);
        mapView.getOverlays().add(marker);

        List<GeoPoint> geoPoints = new ArrayList<>();
        Polyline polyline = new Polyline();
        polyline.setWidth(5.0f);
        geoPoints.add(new GeoPoint(Double.parseDouble(userLatlng.split(":")[0]), Double.parseDouble(userLatlng.split(":")[1])));

        List<ArrayList<String>> list = new ArrayList<>();
        int urutan = 0;
        Iterator<String> iter = dataObject.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            JSONObject object = new JSONObject(dataObject.getString("data" + urutan));
            String distance = object.getString("distance");
            String latlng = object.getJSONArray("data2").getJSONObject(0).getString("latlng");
            name = object.getJSONArray("data2").getJSONObject(0).getString("name");
            if (!userLatlng.equals(latlng)) {
                geoPoints.add(new GeoPoint(Double.parseDouble(latlng.split(":")[0]), Double.parseDouble(latlng.split(":")[1])));
                marker = new Marker(mapView);
                marker.setPosition(new GeoPoint(Double.parseDouble(latlng.split(":")[0]), Double.parseDouble(latlng.split(":")[1])));
                marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                marker.setIcon(DataDetailZakatActivity.this.getResources().getDrawable(R.drawable.ic_room_blue));
                marker.setTitle(name);
                mapView.getOverlays().add(marker);

                ArrayList<String> dlist = new ArrayList<>();
                dlist.add(name);
                dlist.add(String.format("%.2f km", Double.parseDouble(distance)));
                list.add(dlist);
            }
            urutan++;
            if (urutan == totaldata)
                break;
        }
        mAdapter.setList(list);
        polyline.setPoints(geoPoints);
        mapView.getOverlays().add(polyline);
    }
}
