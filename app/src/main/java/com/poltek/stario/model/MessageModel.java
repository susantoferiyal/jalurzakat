package com.poltek.stario.model;

import java.io.Serializable;

public class MessageModel implements Serializable {

    public MessageModel() {

    }

    public MessageModel(String id_trans, String id_zakat, String tipe_trans, String zakat_keluar, String zakat_masuk,
                        String mas_terima, String nama_mas_terima, String mas_beri, String nama_mas_beri, String tanggal) {
        this.id_trans = id_trans;
        this.id_zakat = id_zakat;
        this.tipe_trans = tipe_trans;
        this.zakat_keluar = zakat_keluar;
        this.zakat_masuk = zakat_masuk;
        this.mas_terima = mas_terima;
        this.nama_mas_terima = nama_mas_terima;
        this.mas_beri = mas_beri;
        this.nama_mas_beri = nama_mas_beri;
        this.tanggal = tanggal;
    }

    String id_trans;
    String id_zakat;
    String tipe_trans;
    String zakat_keluar;
    String zakat_masuk;
    String mas_terima;
    String nama_mas_terima;
    String mas_beri;
    String nama_mas_beri;
    String tanggal;

    public String getId_trans() {
        return id_trans;
    }

    public void setId_trans(String id_trans) {
        this.id_trans = id_trans;
    }

    public String getId_zakat() {
        return id_zakat;
    }

    public void setId_zakat(String id_zakat) {
        this.id_zakat = id_zakat;
    }

    public String getTipe_trans() {
        return tipe_trans;
    }

    public void setTipe_trans(String tipe_trans) {
        this.tipe_trans = tipe_trans;
    }

    public String getZakat_keluar() {
        return zakat_keluar;
    }

    public void setZakat_keluar(String zakat_keluar) {
        this.zakat_keluar = zakat_keluar;
    }

    public String getZakat_masuk() {
        return zakat_masuk;
    }

    public void setZakat_masuk(String zakat_masuk) {
        this.zakat_masuk = zakat_masuk;
    }

    public String getMas_terima() {
        return mas_terima;
    }

    public void setMas_terima(String mas_terima) {
        this.mas_terima = mas_terima;
    }

    public String getNama_mas_terima() {
        return nama_mas_terima;
    }

    public void setNama_mas_terima(String nama_mas_terima) {
        this.nama_mas_terima = nama_mas_terima;
    }

    public String getMas_beri() {
        return mas_beri;
    }

    public void setMas_beri(String mas_beri) {
        this.mas_beri = mas_beri;
    }

    public String getNama_mas_beri() {
        return nama_mas_beri;
    }

    public void setNama_mas_beri(String nama_mas_beri) {
        this.nama_mas_beri = nama_mas_beri;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
