package com.poltek.stario.model;

import java.io.Serializable;

public class ReportModel implements Serializable {

    public ReportModel() {

    }

    public ReportModel(String mas_beri, String alamat_mas, String muzaki, String mustahiq, String total, String status, String zakat_masuk, String nama_mas_terima, String nama_mas_beri, String tanggal) {
        this.mas_beri = mas_beri;
        this.alamat_mas = alamat_mas;
        this.muzaki = muzaki;
        this.mustahiq = mustahiq;
        this.total = total;
        this.status = status;
        this.zakat_masuk = zakat_masuk;
        this.nama_mas_terima = nama_mas_terima;
        this.nama_mas_beri = nama_mas_beri;
        this.tanggal = tanggal;
    }

    String mas_beri;
    String alamat_mas;
    String muzaki;
    String mustahiq;
    String total;
    String status;
    String zakat_masuk;
    String nama_mas_terima;
    String nama_mas_beri;
    String tanggal;

    public String getMas_beri() {
        return mas_beri;
    }

    public void setMas_beri(String mas_beri) {
        this.mas_beri = mas_beri;
    }

    public String getAlamat_mas() {
        return alamat_mas;
    }

    public void setAlamat_mas(String alamat_mas) {
        this.alamat_mas = alamat_mas;
    }

    public String getMuzaki() {
        return muzaki;
    }

    public void setMuzaki(String muzaki) {
        this.muzaki = muzaki;
    }

    public String getMustahiq() {
        return mustahiq;
    }

    public void setMustahiq(String mustahiq) {
        this.mustahiq = mustahiq;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZakat_masuk() {
        return zakat_masuk;
    }

    public void setZakat_masuk(String zakat_masuk) {
        this.zakat_masuk = zakat_masuk;
    }

    public String getNama_mas_terima() {
        return nama_mas_terima;
    }

    public void setNama_mas_terima(String nama_mas_terima) {
        this.nama_mas_terima = nama_mas_terima;
    }

    public String getNama_mas_beri() {
        return nama_mas_beri;
    }

    public void setNama_mas_beri(String nama_mas_beri) {
        this.nama_mas_beri = nama_mas_beri;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
