package com.poltek.stario.model;

import java.io.Serializable;

public class DataZakatModel implements Serializable {

    public DataZakatModel() {

    }

    public DataZakatModel(String id, String name, String muzaki,String mustahiq, String total, String status, String latlng) {
        this.id = id;
        this.name = name;
        this.muzaki = muzaki;
        this.mustahiq = mustahiq;
        this.total = total;
        this.status = status;
        this.latlng = latlng;
    }

    String id;
    String name;
    String muzaki;
    String mustahiq;
    String total;
    String status;
    String latlng;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMuzaki() {
        return muzaki;
    }

    public void setMuzaki(String muzaki) {
        this.muzaki = muzaki;
    }

    public String getMustahiq() {
        return mustahiq;
    }

    public void setMustahiq(String mustahiq) {
        this.mustahiq = mustahiq;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", muzaki='" + muzaki + '\'' +
                ", mustahiq='" + mustahiq + '\'' +
                ", total='" + total + '\'' +
                ", status='" + status + '\'' +
                ", latlng='" + latlng + '\'' +
                '}';
    }
}
