package com.poltek.stario;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.poltek.stario.fragment.DataZakatFragment;
import com.poltek.stario.fragment.HomeFragment;
import com.poltek.stario.fragment.InputZakatFragment;
import com.poltek.stario.fragment.MapFragment;
import com.poltek.stario.fragment.MessageFragment;
import com.poltek.stario.fragment.OsmMapFragment;
import com.poltek.stario.util.ZakatSharedPreferences;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView tv_username;
    private DrawerLayout mDrawer;
    private GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDrawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        tv_username = headerLayout.findViewById(R.id.tv_username);
        tv_username.setText(ZakatSharedPreferences.getSharedPreference(HomeActivity.this, "nama_masjid", "-"));

        displaySelectedScreen(R.id.nav_home);
    }

    private void displaySelectedScreen(int itemId) {
        int id = itemId;

        Fragment fragment = null;

        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
            setTitle("Home");
        } else if (id == R.id.nav_input) {
            fragment = new InputZakatFragment();
            setTitle("Input Zakat");
        } else if (id == R.id.nav_data) {
            fragment = new DataZakatFragment();
            setTitle("Data Zakat");
        } else if (id == R.id.nav_map) {
            fragment = new OsmMapFragment();
            setTitle("Lokasi Masjid");
        } else if (id == R.id.nav_pesanapps) {
            fragment = new MessageFragment();
            setTitle("Pesan");
        } else if (id == R.id.nav_logout) {
            ZakatSharedPreferences.clearSharedPreference(HomeActivity.this);
            finishAffinity();
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        mDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        displaySelectedScreen(menuItem.getItemId());
        return true;
    }
}
