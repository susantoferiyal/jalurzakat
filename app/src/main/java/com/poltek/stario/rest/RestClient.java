package com.poltek.stario.rest;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.poltek.stario.util.Constant;

public class RestClient extends AsyncHttpClient {

    private static AsyncHttpClient mClient = null;
    private static int mTimeout = 1000*15;

    public static void get(Context context, String url, AsyncHttpResponseHandler handler) {
        mClient = new AsyncHttpClient();
        mClient.setTimeout(mTimeout);
        try {
            mClient.get(context, getAbsoluteUrl(url), handler);
        } catch (Exception e) {

        }
    }

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler handler) {
        Log.d("RestClient", "url " + getAbsoluteUrl(url) + ", params " + params.toString());
        mClient = new AsyncHttpClient();
        mClient.setTimeout(mTimeout);
        try {
            mClient.post(context, getAbsoluteUrl(url), params, handler);
        } catch (Exception e) {

        }
    }

    public static void put(Context context, String url, RequestParams params, AsyncHttpResponseHandler handler) {
        mClient = new AsyncHttpClient();
        mClient.setTimeout(mTimeout);
        try {
            mClient.put(context, getAbsoluteUrl(url), params, handler);
        } catch (Exception e) {

        }
    }

    public static String getAbsoluteUrl(String url) {
        return Constant.REST_URL + url + ".php";
    }
}
